﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace Word2Excel
{
    public partial class mF : Form
    {
        public mF()
        {
            InitializeComponent();
        }

        private void btnC_Click(object sender, EventArgs e)
        {
            //todo校验
            if (File.Exists(textBox1.Text))
            {
                MessageBox.Show("目标文件不存在！");
            }
            //还有很多校验 balabal
            Convertor c = new Convertor();
            c.Convert2CSV(textBox1.Text,textBox2.Text);
            MessageBox.Show("结束");
        }

        private void btnF_Click(object sender, EventArgs e)
        {
            OpenFileDialog fdg = new OpenFileDialog();

            fdg.Filter = "Word Files(*.doc;*.docx;)|*.doc;*.docx";
            fdg.RestoreDirectory = true;

            if (fdg.ShowDialog() == DialogResult.OK)
            {
                textBox1.Text = fdg.FileName;
            }
        }

        private void btnD_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog dlgOpenFolder = new FolderBrowserDialog();
            dlgOpenFolder.RootFolder = Environment.SpecialFolder.MyComputer;
            dlgOpenFolder.ShowNewFolderButton = true;
            DialogResult result = dlgOpenFolder.ShowDialog(this);
            if (result == DialogResult.OK)
            {
                textBox2.Text = dlgOpenFolder.SelectedPath;
            }
        }
    }
}
