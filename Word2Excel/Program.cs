﻿using System;
using System.Linq;
using System.IO;
using System.Windows.Forms;

namespace Word2Excel
{
    public class Program
    {
        /// <summary>
        /// 应用程序的主入口点。
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new mF());
        }
    }
}
