﻿using System;
using Aspose.Words;
using System.IO;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Text;

namespace Word2Excel
{
    public class Convertor
    {
        #region 变量
        int _lastlevel = 10;
        Dictionary<string, string> _tags = new Dictionary<string, string>();
        string currentItem = string.Empty;
        bool startParse = false;
        bool _catalog = true;
        int _tableindex = 0; //标记是不是头表
        #endregion

        public Convertor()
        {
            #region aspose注册
            try
            {
                License lic = new License();
                string licPath = Directory.GetCurrentDirectory() + "\\Lic\\Aspose.Words.lic";
                lic.SetLicense(licPath);
            }
            catch
            {
                //出错
                throw new Exception(@"Aspose注册失败");
            }
            #endregion
        }

        internal void Convert2CSV()
        {
            Document doc = new Document(@"E:\git\word2excel\Word2Excel\test\附件1 第十七期节能产品政府采购清单.docx");
            NodeCollection elements = doc.GetChildNodes(NodeType.Any, true);

            //table名称先入 出
            foreach (Node element in elements)
            {
                if (element.NodeType == NodeType.Table && startParse)
                {
                    //遇到table 则表明过了目录期限 不再更新 _tags
                    _catalog = false;
                    //todo
                    if (string.IsNullOrEmpty(currentItem))
                        throw new Exception("文件无法创建，word内容格式");

                    StringBuilder sb = new StringBuilder();
                    Dictionary<string, string> coltmp = new Dictionary<string, string>();

                    Aspose.Words.Tables.Table table = (Aspose.Words.Tables.Table)element;
                    foreach (Aspose.Words.Tables.Row row in table)
                    {
                        //由于品牌转换会出现问题所以目前来说进行手动
                        //首先为1、2列 再 4、5 其余都为第三列内容
                        int celCnt = row.Cells.Count;
                        //if (celCnt < 5) continue;
                        String Col0 = TextFormatClear(row.Cells[0].GetText());
                        if (string.IsNullOrEmpty(Col0))
                        {
                            Col0 = coltmp["制造商"];
                        }
                        else
                        {
                            coltmp["制造商"] = Col0;
                        }

                        string Col1 = TextFormatClear(row.Cells[1].GetText());
                        if (string.IsNullOrEmpty(Col1))
                        {
                            Col1 = coltmp["品牌"];
                        }
                        else
                        {
                            coltmp["品牌"] = Col1;
                        }

                        //倒数俩
                        string Col3 = TextFormatClear(row.Cells[celCnt - 2].GetText());
                        if (string.IsNullOrEmpty(Col3))
                        {
                            Col3 = coltmp["节字标志"];
                        }
                        else
                        {
                            coltmp["节字标志"] = Col3;
                        }

                        string Col4 = TextFormatClear(row.Cells[celCnt - 1].GetText());
                        if (string.IsNullOrEmpty(Col4))
                        {
                            Col4 = coltmp["节能产品"];
                        }
                        else
                        {
                            coltmp["节能产品"] = Col4;
                        }

                        //一般情况型号都有 不动了
                        string Col2 = string.Empty;
                        for (int i = 2; i < row.Cells.Count - 2; i++)
                        {
                            Col2 += TextFormatClear(row.Cells[i].GetText());
                        }

                        string rowTxt = Col0 + "," + Col1 + "," + Col2 + "," + Col3 + "," + Col4;
                        sb.AppendLine(rowTxt);
                    }
                    File.AppendAllText(@"D:/节能/" + currentItem + ".csv", sb.ToString(), Encoding.UTF8);
                }

                //终结困难 这部分不做了
                if (element.NodeType == NodeType.Paragraph)
                {
                    if (string.IsNullOrEmpty(element.GetText()))
                        continue;
                    Dictionary<string, string> dic = AnalysisCatalog(element.GetText());

                    foreach (var kv in dic)
                    {
                        if (_tags.ContainsKey(kv.Key))
                        {
                            startParse = _tags[kv.Key] == "s" ? true : false;
                            currentItem = kv.Key;
                        }
                        else
                        {
                            //不存在 并且在目录页
                            if (_catalog)
                            {
                                _tags.Add(kv.Key, kv.Value);
                            }
                        }
                    }
                }
            }
        }

        internal void Convert2CSV(string FileName, string tarFolder)
        {
            Document doc = new Document(FileName);
            NodeCollection elements = doc.GetChildNodes(NodeType.Any, true);

            //table名称先入 出
            foreach (Node element in elements)
            {
                if (element.NodeType == NodeType.Table && startParse)
                {
                    //遇到table 则表明过了目录期限 不再更新 _tags
                    _catalog = false;

                    if (string.IsNullOrEmpty(currentItem))
                        throw new Exception("文件无法创建，word内容格式");

                    StringBuilder sb = new StringBuilder();
                    Dictionary<string, string> coltmp = new Dictionary<string, string>();

                    Aspose.Words.Tables.Table table = (Aspose.Words.Tables.Table)element;
                    foreach (Aspose.Words.Tables.Row row in table)
                    {
                        //由于品牌转换会出现问题所以目前来说进行手动
                        //首先为1、2列 再 4、5 其余都为第三列内容
                        int celCnt = row.Cells.Count;
                        //if (celCnt < 5) continue;
                        String Col0 = TextFormatClear(row.Cells[0].GetText());
                        //去表头
                        if (_tableindex != 0 && Col0 == "制造商")
                            continue;
                        if (string.IsNullOrEmpty(Col0))
                        {
                            Col0 = coltmp["制造商"];
                        }
                        else
                        {
                            coltmp["制造商"] = Col0;
                        }

                        string Col1 = TextFormatClear(row.Cells[1].GetText());
                        if (string.IsNullOrEmpty(Col1))
                        {
                            Col1 = coltmp["品牌"];
                        }
                        else
                        {
                            coltmp["品牌"] = Col1;
                        }

                        //倒数俩
                        string Col3 = TextFormatClear(row.Cells[celCnt - 2].GetText());
                        if (string.IsNullOrEmpty(Col3))
                        {
                            Col3 = coltmp["节字标志"];
                        }
                        else
                        {
                            coltmp["节字标志"] = Col3;
                        }

                        string Col4 = TextFormatClear(row.Cells[celCnt - 1].GetText());
                        if (string.IsNullOrEmpty(Col4))
                        {
                            Col4 = coltmp["节能产品"];
                        }
                        else
                        {
                            coltmp["节能产品"] = Col4;
                        }

                        //一般情况型号都有 不动了
                        string Col2 = string.Empty;
                        for (int i = 2; i < row.Cells.Count - 2; i++)
                        {
                            Col2 += TextFormatClear(row.Cells[i].GetText());
                        }

                        string rowTxt = Col0 + "," + Col1 + "," + Col2 + "," + Col3 + "," + Col4;
                        sb.AppendLine(rowTxt);
                    }
                    File.AppendAllText(tarFolder + "\\" + currentItem + ".csv", sb.ToString(), Encoding.UTF8);

                    //不是头表后面的
                    _tableindex = 1;
                }

                //终结困难 这部分不做了
                if (element.NodeType == NodeType.Paragraph)
                {
                    if (string.IsNullOrEmpty(element.GetText()))
                        continue;
                    Dictionary<string, string> dic = AnalysisCatalog(element.GetText());

                    foreach (var kv in dic)
                    {
                        if (_tags.ContainsKey(kv.Key))
                        {
                            startParse = _tags[kv.Key] == "s" ? true : false;
                            currentItem = kv.Key;
                            _tableindex = 0;
                        }
                        else
                        {
                            //不存在 并且在目录页
                            if (_catalog)
                            {
                                _tags.Add(kv.Key, kv.Value);
                            }
                        }
                    }
                }
            }
        }

        private string TextFormatClear(string content)
        {
            content = content.Replace("\n", "").Replace("\r", "");
            string pattern = @"[\w\u4E00-\u9FFF][\w\s\-\u4E00-\u9FFF]+";
            Regex reg = new Regex(pattern);
            return reg.Match(content).Value;
        }

        /// <summary>
        /// 分析目录
        /// </summary>
        /// <param name="content"></param>
        /// <returns></returns>
        private Dictionary<string, string> AnalysisCatalog(string content)
        {
            //分析目录 
            string pattern1 = @"(?<=\d\.)[★\s]{0,3}\w+\s[\u4E00-\u9FFF（）、]+";
            string pattern2 = @"(?<=（\d）)[★\s]{0,3}(?:\w+)?\s?[\u4E00-\u9FFF（）、]+";
            string pattern3 = @"(?<=[①②③④⑤⑥])[★\s]{0,3}(?:\w+)?\s?[\u4E00-\u9FFF（）、]+";

            Dictionary<string, string> rs = new Dictionary<string, string>();

            Regex reg1 = new Regex(pattern1);
            Regex reg2 = new Regex(pattern2);
            Regex reg3 = new Regex(pattern3);

            string c3 = reg3.Match(content).Value.Replace(" ", "");
            string c2 = reg2.Match(content).Value.Replace(" ", "");
            string c1 = reg1.Match(content).Value.Replace(" ", "");

            /***文档的低级目录可能会和1级目录窜行，所以先底层目录再高层目录***/

            //3
            if (!string.IsNullOrEmpty(c3))
            {
                if (_lastlevel < 3 && _lastlevel != -1 && _lastlevel != -2)
                {
                    rs.Add(c3, "s");
                }
                else if (c3.Contains("★"))
                {
                    _lastlevel = 3;
                    rs.Add(c3, "s");
                }
                else
                {
                    rs.Add(c3, "e");
                }
            }

            //2
            if (!string.IsNullOrEmpty(c2))
            {
                //优先判断上级是否打★
                if (_lastlevel < 2 && _lastlevel != -1 && _lastlevel != -2)
                {
                    rs.Add(c2, "s");
                }
                else if (c2.Contains("★"))
                {
                    _lastlevel = 2;
                    rs.Add(c2, "s");
                }
                else
                {
                    _lastlevel = -2;
                    rs.Add(c2, "e");
                }
            }

            //1
            if (!string.IsNullOrEmpty(c1))
            {
                if (c1.Contains("★"))
                {
                    _lastlevel = 1;
                    rs.Add(c1, "s");
                }
                else
                {
                    _lastlevel = -1;
                    rs.Add(c1, "e");
                }
            }
            return rs;
        }
    }
}
