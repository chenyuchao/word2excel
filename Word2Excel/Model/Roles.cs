﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Word2Excel.Model
{
    public class Roles
    {
        public Roles() { }

        //来三个实例
        public Role CUnit = new Role() { Name = "承建单位", RoleType = 1, ColIndex = -1 };
        public Role PUnit = new Role() { Name = "参建单位", RoleType = 2, ColIndex = -1 };
        public Role SUnit = new Role() { Name = "监理单位", RoleType = 3, ColIndex = -1 };

        public struct Role
        {
            public string Name;
            public int RoleType;
            public int ColIndex;
        }
    }
}
