﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TBKC_Word2Excel.Model
{
    public class WordDocumentTable
    {
        public WordDocumentTable(int PiTableID)
        {
            MiTableID = PiTableID;
        }

        public WordDocumentTable(int PiTableID, int PiColumnID)
        {
            MiTableID = PiTableID;
            MiColumnID = PiColumnID;
        }

        public WordDocumentTable(int PiTableID, int PiColumnID, int PiRowID)
        {
            MiTableID = PiTableID;
            MiColumnID = PiColumnID;
            MiRowID = PiRowID;
        }

        private int MiTableID = 0;

        public int TableID
        {
            get { return MiTableID; }
            set { MiTableID = value; }
        }

        private int MiRowID = 0;
        public int RowID
        {
            get { return MiRowID; }
            set { MiRowID = value; }
        }

        private int MiColumnID = 0;
        public int ColumnID
        {
            get { return MiColumnID; }
            set { MiColumnID = value; }
        }

        public List<WordDocumentTable> WordDocumentTables
        {
            get
            {
                List<WordDocumentTable> wordDocTable = new List<WordDocumentTable>();
                //Reads the data from the first Table of the document.    
                wordDocTable.Add(new WordDocumentTable(0));
                //Reads the data from the second table and its second column. 
                //This table has only one row.    
                wordDocTable.Add(new WordDocumentTable(1, 1));
                //Reads the data from third table, second row and second cell.    
                wordDocTable.Add(new WordDocumentTable(2, 1, 1));
                return wordDocTable;
            }
        }

    }
}


